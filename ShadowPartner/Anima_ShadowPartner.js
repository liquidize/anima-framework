/*=============================================================================
 * Anima - Shadow Partner
 * By Liquidize - http://Mintkit.lol
 * Anima_ShadowPartner.js
 * Version: 1.12
 * Free for commercial and non commercial use upon credit of "Anima Framework" or
 * Liquidize.
 *=============================================================================*/
/*:
 * @plugindesc Plugin Description <Anima_ShadowPartner>
 * @author Liquidize
  *
 * @param xOffset
 * @desc The offset in pixels on the X axis in which shadow sprites are shown from eachother.
 * (e.g: The shadows are 32px behind the actor, and eachother.)
 * @default 48
 *
 * @param maxShadows
 * @desc The maximum amount of shadows an actor or enemy can have by default.
 * @default 3
 *
 * @param certainHitBypass
 * @desc do certain hit type attacks ignore the shadows?
 * @default true
 *
 * @param useSpriteWidth
 * @desc When placing the shadows, should we place them using the battler's sprite width as an offset? Requires
 * YEP_BattleEngineCore
 * @default true
 *
 * @param defaultTint
 * @desc the default color tint of the shadows. (default is black)
 * @default #000000
 *
 * @help
 * ============================================================================
 * Liquidize's Plugins - The Anima Framework
 * ============================================================================
 * The Anima Framework is my contribution to the RPG Maker MV Community.
 * It consist of script request that I find interesting, or just want too make.
 * There is no set theme to these scripts other than the fact that they're almost
 * all from the request section.
 * 
 * Shadow Partner is a script which upon given a state that is configured to give 'shadows'
 * the user becomes immune to damage against hits. The shadows instead take the hit
 * resulting in 0 damage, but the shadow is lost. When all shadows are lost the
 * user loses the effect.
 * 
 * More information can be found in the request thread here: 
 * http://forums.rpgmakerweb.com/index.php?/topic/50918-blink-state/
 *
 * ============================================================================
 * Giving States Shadows
 * ============================================================================
 * Use the following note tags to give states shadows.
 *
 * <Shadow Count: x> - where x is a number, gives the person affected with this state
 * x amount of shadows.
 *
 * <Shadow Chance: 0-100%> - where 0-100% is a percentage (e.g: 25%) this is the chance
 * which shadows activate when hit. You can leave this out for the default 100%
 *
 * <Shadow Tint: HEXCOLORHERE> - where HEXCOLORHERE is any hexadecimal color like
 * #000000 (black) or #FFFFFF (white). Changes the color of the shadow.
 *
 * =============================================================================
 * Setting max amount of shadows
 * =============================================================================
 *
 * Use the following notetags to set the maximum amount of shadows Actors and
 * enemies can have. NOTE: Battlers can only be affected by 1 type of shadow state
 * at a time however.
 *
 * <Max Shadows: x> - where x is the maximum amount of shadows this battler can have
 * ===========================================================================
 * By passing shadows
 * ===========================================================================
 *
 * The parameter certainHitBypass if set to true, will allow all items/skills with
 * the certain hit, hit type to bypass shadows automatically. Any other ability
 * or item that you want to bypass the targets shadows just give the following tag.
 *
 * <Bypass Shadows>
 * ============================================================================
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 *
 * Version 1.11:
 *  - Changed how shadows are drawn.
 *  - Fixed an issue which caused shadows to attack instead of the
 *    main sprite when using Yanflys battle engine
 *  - Added Shadow Tint tag to change the color of shadows based off state.
 *  - Added a new parameter 'useSpriteWidth', when true it will use the width
 *    of the sprite to determine its offset.
 *
 * Version 1.1:
 * - Changed how shadows are given, now you are able to use note tags on any state.
 * - Added the ability to bypass shadows.
 * - Added the ability to set the maximum amount of shadows an actor can have.
 *
 * ============================================================================
 *
 * Check out my website:
 * http://mintkit.lol
 * 
 *=============================================================================*/
var Imported = Imported || {};
var Anima = Anima || {};
Anima.ShadowPartner = Anima.ShadowPartner || {};

(function ($) {
    "use strict";

    var parameters = $plugins.filter(function (plugin) {
        return plugin.description.contains('<Anima_ShadowPartner>');
    });
    if (parameters.length === 0) {
        throw new Error("Couldn't find Anima_ShadowPartner parameters.");
    }
    $.Parameters = parameters[0].parameters;
    $.Param = {};

    // Param validation
    $.Param.xOffset = Number($.Parameters.xOffset || 32);
    $.Param.maxShadows = Number($.Parameters.maxShadows || 3);
    $.Param.certainHitBypass = eval($.Parameters.certainHitBypass);
    $.Param.useSpriteWidth = eval($.Parameters.useSpriteWidth);
    $.Param.defaultTint = String($.Parameters.defaultTint);

    // Note tags.
    var shadowDatabaseLoaded = DataManager.isDatabaseLoaded;
    DataManager.isDatabaseLoaded = function () {
        if (!shadowDatabaseLoaded.call(this)) return false;
        this.processShadowBypassNotetags($dataSkills);
        this.processShadowBypassNotetags($dataItems);
        this.processShadowStateNotetags($dataStates);
        this.processMaxShadowsNotetags($dataActors);
        return true;
    };

    DataManager.processShadowBypassNotetags = function (group) {
        var note1 = /<(?:BYPASS SHADOWS|ignore shadows)>/i;
        for (var n = 1; n < group.length; n++) {
            var obj = group[n];
            var notedata = obj.note.split(/[\r\n]+/);

            obj.bypassshadows = false;

            for (var i = 0; i < notedata.length; i++) {
                var line = notedata[i];
                if (line.match(note1)) {
                    obj.bypassshadows = true;
                }
            }
        }
    };

    DataManager.processShadowStateNotetags = function (group) {
        var note1 = /<(?:Shadow Count|shadows):[ ](\d+)>/i;
        var note2 = /<(?:Shadow Chance|shadow per):[ ](\d+)([%ï¼…])>/i;
        var note3 = /<(?:Shadow Tint):[ ](#[0-9a-fA-F]+)>/i;
        for (var n = 1; n < group.length; n++) {
            var obj = group[n];
            var notedata = obj.note.split(/[\r\n]+/);

            obj.shadowcount = 0;
            obj.shadowper = 1.0;
            obj.shadowtint = $.Param.defaultTint.replace("#","0x");

            for (var i = 0; i < notedata.length; i++) {
                var line = notedata[i];
                if (line.match(note1)) {
                    obj.shadowcount = parseInt(RegExp.$1);
                } else if (line.match(note2)) {
                    obj.shadowper = parseFloat(RegExp.$1 * 0.01);
                } else if (line.match(note3)){
                    obj.shadowtint = RegExp.$1.replace("#","0x");
                }
            }

        }

    };

    DataManager.processMaxShadowsNotetags = function (group) {
        var note1 = /<(?:Max Shadows|shadow max):[ ](\d+)>/i;
        for (var n = 1; n < group.length; n++) {
            var obj = group[n];
            var notedata = obj.note.split(/[\r\n]+/);

            obj.maxshadows = $.Param.maxShadows;
            for (var i = 0; i < notedata.length; i++) {
                var line = notedata[i];
                if (line.match(note1)) {
                    obj.maxshadows = parseInt(RegExp.$1);
                }
            }
        }
    };

    var shadowPartnerActorInit = Game_Actor.prototype.initMembers;
    Game_Actor.prototype.initMembers = function() {
        shadowPartnerActorInit.call(this);
        this.currentshadows = 0;
    };

    var shadowPartnerActorSetup = Game_Actor.prototype.setup;
    Game_Actor.prototype.setup = function(actorId){

        shadowPartnerActorSetup.call(this,actorId);
        var actor = $dataActors[actorId];

        this.maxshadows = actor.maxshadows;
    };

    var shadowPartnerGameEnemyInit = Game_Enemy.prototype.initMembers;
    Game_Enemy.prototype.initMembers = function() {
        shadowPartnerGameEnemyInit.call(this);
        this.currentshadows = 0;
    };

    var shadowPartnerEnemySetup = Game_Enemy.prototype.setup;
    Game_Enemy.prototype.setup = function(enemyId, x, y) {
        this.maxshadows = $dataEnemies[enemyId].maxshadows;
        shadowPartnerEnemySetup.call(this,enemyId,x,y);
    };

    // Battle start, add the shadows if they exists.
    var shadowPartnerBattleStart = Game_Battler.prototype.onBattleStart;
    Game_Battler.prototype.onBattleStart = function () {
        if (this.currentshadows > 0) {
            this.addShadowSprites();
        }
        shadowPartnerBattleStart.call(this);
    };

    Game_Battler.prototype.getShadowTint = function(){
        for (var i = 0; i < this._states.length; i++){
            if ($dataStates[this._states[i]].shadowcount > 0){
                return $dataStates[this._states[i]].shadowtint;
            }
        }
        return $.Param.defaultTint;
    };

    Game_Battler.prototype.addShadowSprites = function(){
        var xoffset = $.Param.xOffset;
        var tint = this.getShadowTint();
      for (var i = 0; i < this.currentshadows; i++){
          if (this.isActor()) {
              var actorSprites = BattleManager._spriteset._actorSprites;
              var actorSprite = actorSprites[this.index()]
              var sprite = new Sprite_ActorShadow();
              sprite.setBattler(this);
              sprite._shadowIndex = i + 1;
              sprite.setOpacity(255 / this.currentshadows);
              sprite.setTint(tint);
              if ($.Param.useSpriteWidth) {
                  sprite.setHome(actorSprite._homeX + this.battler()._mainSprite.width, actorSprite._homeY);
              } else {
                  sprite.setHome(actorSprite._homeX + xoffset, actorSprite._homeY);
              }
              this.shadowsprites[i + 1] = sprite;
              BattleManager._spriteset._battleField.addChild(sprite);
          } else if (this.isEnemy()) {
              var enemySprites = BattleManager._spriteset._enemySprites;
              var enemySprite = enemySprites[this.index()];
              var sprite = new Sprite_EnemyShadow(this);
              sprite.setBattler(this);
              sprite._shadowIndex = i + 1;
              sprite.setOpacity(255 / this.currentshadows);
              sprite.setTint(tint);
              if ($.Param.useSpriteWidth) {
                  sprite.setHome(enemySprite.x - this.battler().width, enemySprite.y + enemySprite._offsetY);
               } else {
                  sprite.setHome(enemySprite.x - xoffset, enemySprite.y + enemySprite._offsetY);
              }
              this.shadowsprites[i + 1] = sprite;
              BattleManager._spriteset._battleField.addChild(sprite);
          }
      }
    };

    // When the shadow partner state should be added.
    var shadowPartneraddState = Game_Battler.prototype.addState;
    Game_Battler.prototype.addState = function (stateId) {
        var state = $dataStates[stateId];
        var addshadowcount = state.shadowcount;
        if (state.shadowcount > 0) {
            if (this.currentshadows > 0 && !this.isStateAffected(stateId)) return;
            if ((this.currentshadows + state.shadowcount) > this.maxshadows){
                addshadowcount = this.maxshadows - this.currentshadows;
            }
            this.shadowsprites = this.shadowsprites || [];
            this.currentshadows += addshadowcount;
            shadowPartneraddState.call(this, stateId);
            if ($gameParty.inBattle()) {
                this.addShadowSprites();
            }
        } else {
            shadowPartneraddState.call(this, stateId);
        }
    };

    var shadowPartnerRemoveState = Game_Battler.prototype.removeState;
    Game_Battler.prototype.removeState = function (stateId) {
        var state = $dataStates[stateId];
        if (state.shadowcount > 0) {
            if (this.currentshadows && this.currentshadows > 0) {
                if ($gameParty.inBattle()) {
                    var battleField = BattleManager._spriteset._battleField;
                    for (var i = 1; i < this.shadowsprites.length; i++) {
                        battleField.removeChild(this.shadowsprites[i]);
                        this.shadowsprites[i] = null;
                    }
                }
                this.shadowsprites = [];
                this.currentshadows = 0;
            }
        }
        shadowPartnerRemoveState.call(this, stateId);
    };

    //======================================================================
    // Game Action
    //======================================================================

    var shadowPartnerExecuteDamage = Game_Action.prototype.executeDamage;
    Game_Action.prototype.executeDamage = function (target, value) {
        var damage = value;
        var battleField = BattleManager._spriteset._battleField;
            if ($.Param.certainHitBypass == true) {
                if (this.isCertainHit() !== true) {
                    if (this.bypassShadow() !== true) {
                        if (target.currentshadows && target.currentshadows > 0) {
                            if (this.isForAll()) {
                                for (var i = 0; i < target._states.length; i++) {
                                    if ($dataStates[target._states[i]].shadowcount > 0) {
                                        target.removeState(target._states[i]);
                                    }
                                }
                            }
                            else {
                                if (this.useShadow(target)) {
                                    for (var i = 1; i < target.shadowsprites.length; i++) {
                                        if (target.shadowsprites[i]._shadowIndex === target.currentshadows) {
                                            battleField.removeChild(target.shadowsprites[i]);
                                            target.currentshadows -= 1;
                                            target.shadowsprites[i] = null;
                                            damage = 0;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (target.currentshadows < 1) {
                                for (var i = 0; i < target._states.length; i++) {
                                    if ($dataStates[target._states[i]].shadowcount > 0) {
                                        target.removeState(target._states[i]);
                                    }
                                }

                            }
                        }
                    }
                }
            }
        else {
                if (this.bypassShadow() !== true) {
                    if (target.currentshadows && target.currentshadows > 0) {
                        if (this.isForAll()) {
                            for (var i = 0; i < target._states.length; i++) {
                                if ($dataStates[target._states[i]].shadowcount > 0) {
                                    target.removeState(target._states[i]);
                                }
                            }
                        }
                        else {
                            if (this.useShadow(target)) {
                                for (var i = 1; i < target.shadowsprites.length; i++) {
                                    if (target.shadowsprites[i]._shadowIndex === target.currentshadows) {

                                        battleField.removeChild(target.shadowsprites[i]);
                                        target.currentshadows -= 1;
                                        target.shadowsprites[i] = null;
                                        damage = 0;
                                        break;
                                    }
                                }
                            }
                        }
                        if (target.currentshadows < 1) {
                            for (var i = 0; i < target._states.length; i++) {
                                if ($dataStates[target._states[i]].shadowcount > 0) {
                                    target.removeState(target._states[i]);
                                }
                            }

                        }
                    }
                }
            }
        shadowPartnerExecuteDamage.call(this, target, damage);
    };

    Game_Action.prototype.bypassShadow = function(){
      if (this.item().bypassshadows) return true;
        return false;
    };

    Game_Action.prototype.useShadow = function(target){
        var chance = 1.0;
      for (var i = 0; i < target._states.length; i++){
          if ($dataStates[target._states[i]].shadowcount > 0)
          {
             chance = $dataStates[target._states[i]].shadowper;
              break;
          }
      }
        var randomint =  Math.floor(Math.random() * (100 - 0 + 1)) + 0;
        var randomper = randomint * 0.01;
        if (randomper <= chance) return true;
        return false;
    };

    // convenience function to quickly check if an action is for 'all' enemies.
    Game_Action.prototype.isForAll = function () {
        if (this.isForRandom()) {
            return false;
        }
        if (this.isForOne()) {
            return false;
        }
        return true;
    };


    //=============================================================================
    // Compatibility patch with YEP_BattleEngineCore
    // Checks to make sure the sprite we are requesting to be registered is not
    // an instance of Sprite_ActorShadow or Sprite_EnemyShadow first.
    //=============================================================================

    if (Imported.YEP_BattleEngineCore) {
        BattleManager.registerSprite = function (battler, sprite) {
            if (!this._registeredSprites) this._registeredSprites = {};
            if (battler.isActor()) var id = 100000 + battler.actorId();
            if (battler.isEnemy()) var id = 200000 + battler.index();
            if (sprite instanceof Sprite_ActorShadow || sprite instanceof Sprite_EnemyShadow) return;
            this._registeredSprites[id] = sprite;
        };

    }


    //=============================================================================
    // Sprite_ActorShadow
    //=============================================================================

    function Sprite_ActorShadow() {
        this.initialize.apply(this, arguments);
    }

    Sprite_ActorShadow.prototype = Object.create(Sprite_Actor.prototype);
    Sprite_ActorShadow.prototype.constructor = Sprite_ActorShadow;


    Sprite_ActorShadow.prototype.initMembers = function () {
        Sprite_Battler.prototype.initMembers.call(this);
        this._battlerName = '';
        this._shadowIndex = 0;
        this._motion = null;
        this._motionCount = 0;
        this._pattern = 0;
        this.opacity = 255;
        this.createWeaponSprite();
        this.createShadowSprite();
        this.createMainSprite();
        this.createStateSprite();

    };

    Sprite_ActorShadow.prototype.setOpacity = function(opacity){
      this.opacity = opacity;
    };

    Sprite_ActorShadow.prototype.setTint = function(tint){
        this._mainSprite.tint = tint;
    };



    //=============================================================================
    // Sprite_EnemyShadow
    //=============================================================================

    function Sprite_EnemyShadow() {
        this.initialize.apply(this, arguments);
    }

    Sprite_EnemyShadow.prototype = Object.create(Sprite_Enemy.prototype);
    Sprite_EnemyShadow.prototype.constructor = Sprite_EnemyShadow;

    Sprite_EnemyShadow.prototype.initialize = function (battler) {
        Sprite_Battler.prototype.initialize.call(this, battler);
    };

    Sprite_EnemyShadow.prototype.initMembers = function () {
        Sprite_Battler.prototype.initMembers.call(this);
        this._enemy = null;
        this._appeared = false;
        this._shadowIndex = 0;
        this._battlerName = '';
        this._battlerHue = 0;
        this.opacity = 255;
        this._effectType = null;
        this._effectDuration = 0;
        this._shake = 0;
    };

    Sprite_EnemyShadow.prototype.update = function () {
        Sprite_Battler.prototype.update.call(this);
        if (this._enemy) {
            this.updateEffect();
        }
    };

    Sprite_EnemyShadow.prototype.setBattler = function (battler) {
        Sprite_Battler.prototype.setBattler.call(this, battler);
        this._enemy = battler;
        this.setHome(battler.screenX(), battler.screenY());
    };

    Sprite_EnemyShadow.prototype.revertToNormal = function () {
        this._shake = 0;
        this.blendMode = 0;
        this.opacity = 255;
        this.setBlendColor([0, 0, 0, 0]);
    };

    Sprite_EnemyShadow.prototype.setOpacity = function(opacity){
        this.opacity = opacity;
    };

    Sprite_EnemyShadow.prototype.setTint = function(tint){
        this.tint = tint;
    };


})(Anima.ShadowPartner);

ShadowPartner = Anima.ShadowPartner;
Imported["AnimaShadowPartner"] = 1.12;